<div class="resume-timeline-item-header mb-2">
												    
    <div class="resume-position-meta d-flex justify-content-between mb-1">
        <div class="resume-position-time">Jul 2022 - Present</div>
        <div class="resume-company-name">Self-employment</div>
    </div>
    <h3 class="resume-position-title mb-1">Data Analyst</h3>

</div><!--//resume-timeline-item-header-->
<div class="resume-timeline-item-desc">
    
    
    <ol class="resume-timeline-list" style="text-align: justify">
        
        <li>KPMG Data Analytics Consulting Virtual Internship</li>
            <ul>
                <li>Assessment of data quality and completeness in preparation for analysis (Excel)</li>
                <li>Targeting high value customers based on customer demographics and attributes (A/B Testing)</li>
                <li>Using visualizations to present insights (PowerBI)</li>
            </ul>

        <li>Data Analyst Bootcamp</li>
            <ul>
                <li>Analyzed COVID-19 data in Russia and USA</li>
                <li>Using Docker, Azure Data Studio, SQL, SSMS, PowerBI, Excel</li>
            </ul>
        <li>Portfolio Website</li>
            <ul>
                <li>Building a Portfolio Website Using Bootstrap</li>
            </ul>
        
    </ol>
    
</div><!--//resume-timeline-item-desc-->